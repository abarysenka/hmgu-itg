#!/usr/bin/perl -w
use strict;
use Getopt::Long;

my $vcf;
my $input;

GetOptions("input=s"=>\$input,"vcf=s"=>\$vcf);
usage(),exit unless defined($vcf) && defined($input);

sub usage{
    print "Options: --vcf   <input.vcf>\n";
    print "         --input <input.positions.txt>\n";
}

my $fh;
my %P;
my $C=0;
open($fh, "<",$input) || die "Can't open $input: $!";
while (<$fh>) {
    chomp;
    my @f=split(/\s+/);
    $P{$f[0]}->{$f[1]}=1;
    $C++;
}
close($fh);

$\="\n";
$,="\t";
my $count=0;
open($fh, "<",$vcf) || die "Can't open $vcf: $!";
while (<$fh>) {
    chomp;

    if (/^#/){
	next;
    }

    my @f=split(/\s+/);
    my $chr=$f[0];
    my $pos=$f[1];
    my $id=$f[2];

    next unless defined($P{$chr});

    my $r=$P{$chr};

    next unless defined($r->{$pos});

    print $chr,$pos,$id;
    $count++;

    last if $count==$C;
}
close($fh);
