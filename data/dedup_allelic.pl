#!/usr/bin/perl -w

use strict;

$\="\n";
my %H;
my %dups;
my %chrpos;

while(<STDIN>){
    chomp;
    if (/^#/){
	print $_;
	next;
    }

    my @f=split(/\t/);
    my $chr=$f[0];
    my $pos=$f[1];
    my $ref=$f[3];
    my $alt=$f[4];
    my $str=$chr."_".$pos."_".$ref."_".$alt;

    $H{$str}=$_;
    $dups{$str}++;
    $chrpos{$chr}->{$pos}->{$ref."_".$alt}=1;
}

foreach my $chr (sort {$a <=> $b} keys %chrpos){
    my $r=$chrpos{$chr};
    foreach my $pos (sort {$a <=> $b} keys %$r){
	my $t=$r->{$pos};
	foreach my $k (keys %$t){
	    my $str=$chr."_".$pos."_".$k;
	    die "ERROR: $str" unless exists($dups{$str});
	    my $count=$dups{$str};
	    die "ERROR: $str" unless exists($H{$str});
	    print $H{$str} unless $count>1;
	}
    }
}
