#!/usr/bin/python3.6

import requests, sys
     
server = "http://rest.ensembl.org"

input={}
f = open(sys.argv[1], "r")
for x in f:
    L=x.rstrip().split(" ")
    if L[0] in input:
        input[L[0]].append(int(L[1]))
    else:
        input[L[0]]=[]
        input[L[0]].append(int(L[1]))

for k in input:
    mn=min(input[k])
    mx=max(input[k])
#    print("Working with %s %s %s" % (k,mn,mx))
    ext = "/overlap/region/human/"+k+":"+str(mn)+"-"+str(mx)+"?content-type=application/json;feature=variation"
    r = requests.get(server+ext, headers={ "Content-Type" : "application/json"})
    if not r.ok:
        r.raise_for_status()
        sys.exit()

    decoded = r.json()
    for x in decoded:
        chr=x["seq_region_name"]
        pos=int(x["start"])
        z=input[chr]
        if pos in z:
            print("%s\t%d\t%s" % (x["seq_region_name"],x["start"],x["id"]))
     

