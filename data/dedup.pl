#!/usr/bin/perl -w

use strict;

$\="\n";
my %H;
my %dups;
while(<STDIN>){
    chomp;
    if (/^#/){
	print $_;
	next;
    }

    my @f=split(/\t/);
    my $chr=$f[0];
    my $pos=$f[1];
    if (exists($H{$chr})){
	my $r=$H{$chr};
	if (!exists($r->{$pos})){
	    $r->{$pos}=$_;
	}
	else{
	    $dups{$chr}->{$pos}=1;
	}
    }
    else{
	$H{$chr}->{$pos}=$_;
    }

}

foreach my $chr (sort {$a <=> $b} keys %H){
    my $dr=$dups{$chr};
    my $r=$H{$chr};
    foreach my $pos (sort {$a <=> $b} keys %$r){
	if (!defined($dr)){
	    print $r->{$pos};
	}
	else{
	    my $dr2=$dr->{$pos};
	    print $r->{$pos} unless defined($dr2);
	}
    }
}
